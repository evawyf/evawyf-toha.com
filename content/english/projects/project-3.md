---
title: "Market-Making Trading Strategies"
date: 2020-03-14T15:40:24+06:00
# Project thumb
image : "images/projects/project4.jpg"
draft: false
# description
description: "This is meta description"
---

In the high-frequency trading, the market maker faces an inventory risk due to the diffusive nature of the stocks mid-price and a transactions risk due to a Poisson arrival of market buy and sell order.
Studies of the mechanisms involved in trading financial assets have traditionally focused on quote-driven markets, where a market maker or dealer centralizes buy and sell orders and provides liquidity by setting bid and ask quotes, and an alternative order-driven trading system. The dynamics of a limit order book resembles in many aspects that of a queuing system. Limit orders wait in a queue to be executed against market orders (or canceled).
An important motivation for modelling high-frequency dynamics of order books is to use the information on the current state of the order book to predict its short-term behavior. Therefore, focus on conditional probabilities of events, given the state of the order book.
In this project, I replicate the simulation of the high-frequency trading in a limit order book, (Avellaneda and Stoikov 2006) as understanding process for the mechanism of market making inventory strategy, and further consider the stochastic model for order book dynamics (Cont, Stoikov and Talreja 2009).
Then, focus on the e↵ective of the mid-price trend prediction and the liquidity parameter of Poisson pro- cess. In practical, calculate out the signals by moving average, realized volatility for order risk management, and applicate statistical methods confirm the dynamic spreads and the cancellations.