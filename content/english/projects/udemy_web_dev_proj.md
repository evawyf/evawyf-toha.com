---
title: "Projects in Udemy’s Web Developer Bootcamp"
date: 2020-08-05T15:40:24+06:00
# Project thumb
image : "images/projects/project1.jpg"
draft: false
# description
description: "This is meta description"
---

Working on it. Used React.js, Node.js, Express, GraphQL, and MongoDB, SQL to build payment and yelp-like applications.

#### 1. Photo Gallery v1 Demo 
[Page Link](/demo/photo_gallery/photo_grid.html)

#### 2. Photo Gallery v2 Demo
[Page Link](/demo/photo_gallery_color/gallery.html)