---
title: "Eva W"
image: "images/author.jpg"
email: "evawyf1@gmail.com"
social:
  - icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
    link : "http://facebook.com/evawyf"
  - icon : "ti-linkedin" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.linkedin.com/in/evawyf/"
  - icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
    link : "https://www.instagram.com/evawyf/"
---

evawyf Modified Hugo Theme - GoFolium